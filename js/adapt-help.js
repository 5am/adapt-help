define([ 'core/js/adapt' ], function(Adapt) {
  
  var HelpView = Backbone.View.extend({
    
    initialize: function() {
      this.listenTo(Adapt, {
        'navigation:helpButton': this.onHelpButton,
        'app:languageChanged': this.remove
      }).render();
    },
    
    render: function() {
      var data = this.model.toJSON();
      data._globals = Adapt.course.get('_globals');
      var template = Handlebars.templates.help;
      this.setElement(template(data)).$el.prependTo($('.navigation-inner'));
    },

    onHelpButton: function() {
      var prompt = this.model.get('_notifyPrompt');

      Adapt.trigger('notify:prompt', {
        title: prompt.title,
        body: prompt.body,
        _prompts: [
          {
            promptText: prompt.close
          }
        ]
      });
    }
  });

  function initialise() {
    var config = Adapt.course.get('_help');
    if (!config || !config._isEnabled) return;
    new HelpView({ model: new Backbone.Model(config) });     
  }

  Adapt.once('adapt:start', function() {
    initialise();
        
    Adapt.on('app:languageChanged', function() {
      $(window).off('beforeunload.close');
      // have to wait until the navbar is ready
      Adapt.once('router:location', initialise);
    });
  });
});
