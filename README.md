# adapt-help

An extension to add a help button to display a help popup.

## Installation

* Add the [example JSON](example.json) to `course.json`.
* Run an appropriate Grunt task.

## Usage

* A help button can be added to the navigation bar.
* Prompts using notify can be triggered on this button.

## Attributes

<table>
	<tr>
		<th colspan="3">Attribute<br></th>
		<th>Type</th>
		<th>Description</th>
		<th>Default</th>
	</tr>
	<tr>
		<td colspan="3"><code>_isEnabled</code></td>
		<td>Boolean</td>
		<td>Set to <code>false</code> to completely disable the extension</td>
		<td><code>false</code></td>
	</tr>
	<tr>
		<td colspan="3"><code>title</code></td>
		<td>String</td>
		<td>Prompt title</td>
		<td><code>"Help"</code></td>
	</tr>
	<tr>
		<td colspan="3"><code>body</code></td>
		<td>String</td>
		<td>Prompt message<br></td>
		<td><code>"Help text"</code></td>
	</tr>
	<tr>
		<td colspan="3"><code>close</code></td>
		<td>String</td>
		<td>Close button text<br></td>
		<td><code>"Close"</code></td>
	</tr>
</table>

# Limitations
No known limitations

**Version:** v1.2.0<br>
**Framework versions:** v2.0.4+<br>
**Accessibility support:** WAI AA
